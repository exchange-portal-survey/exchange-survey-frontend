# Exchange Portal Survey UI

This project is the frontend of the Exchange Portal Survey application, that is part of my final apprenticeship project.

## To run project
### Using docker-compose

The survey user interface and the backend API can be run at the same time using the following command:

```bash
docker-compose up -d
```

The API repository must be in the same directory as this project for this command to work.

### Running independent of API

To run the UI without any backend functionality, you **must run the following build command first**:

```bash
npm run build
```

- This command will bundle the JavaScript and build all GDS assets
- Once the project is built, it can be run within a Docker container using the following commands, which will expose the frontend at `http://localhost:8080`

```bash
docker build . -t survey-ui
docker run -d -p 8080 survey-ui
```

- Alternatively, a `live-server` or any other alternative hosting option should run the frontend, although this is not encouraged
