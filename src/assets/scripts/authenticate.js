const { checkIfAuthorised } = require('./survey-management');

async function authenticate(userInput) {
  const requestObject = {
    password: userInput,
  };
  const response = await fetch('http://localhost:3000/survey/password/verify', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(requestObject),
  });
  const isAuthenticated = await response.json();
  return (isAuthenticated);
}

function getUserInput() {
  return document.getElementById('password').value;
}

function setAuthToken() {
  localStorage.setItem('exchange-survey-token', new Date().toISOString());
}

async function initAuth() {
  if (checkIfAuthorised()) {
    window.location.href = '/surveyResponses';
  }
  const submitButton = document.getElementById('submit-button');
  submitButton.addEventListener('click', async () => {
    const userInput = getUserInput();
    try {
      const isVerified = await authenticate(userInput);
      if (isVerified) {
        setAuthToken();
        window.location.href = '/surveyResponses';
      } else {
        const errorMessage = 'Incorrect password';
        const errorMessageElem = document.getElementById('error-message');
        errorMessageElem.innerText = errorMessage;
      }
    } catch (err) {
      console.dir(err);
    }
  });
}

module.exports = {
  authenticate,
  setAuthToken,
  getUserInput,
  initAuth,
};
