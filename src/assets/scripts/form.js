function getContactDetails() {
  const email = document.getElementById('email-address').value;
  const role = document.getElementById('role').value;
  return {
    email,
    role,
  };
}

function generateResponseObject(response, questionNumber) {
  const questionTextCorrectWhiteSpace = response.querySelector('h3')
    .textContent.replace(/\s+/g, ' ').trim();
  const questionResponse = response.querySelector('.app-survey__response')
    .querySelector(`input[name=question-${questionNumber}]:checked`)?.value;
  return {
    question: questionTextCorrectWhiteSpace,
    response: questionResponse,
  };
}

function getFormResponses() {
  const response = [];
  const questions = document.getElementsByClassName('app-survey__question');

  [...questions].forEach((question, index) => {
    const questionNumber = index + 1;
    response.push(generateResponseObject(question, questionNumber));
  });

  return response;
}

function getFeedback() {
  return document.getElementById('feedback')?.value;
}

function createForm(contactDetails, formResponses, getFeedbackFunction) {
  return {
    contact: contactDetails,
    responses: formResponses,
    feedback: getFeedbackFunction(),
  };
}

function formIsUnpopulated(form) {
  const { responses, feedback } = form;
  const feedbackIsUnpopulated = (feedback === '' || feedback === null);
  const allResponsesUnpopulated = responses.every((response) => response?.response === undefined);
  return feedbackIsUnpopulated && allResponsesUnpopulated;
}

function sendForm(form) {
  return fetch('http://localhost:3000/survey', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(form),
  });
}

function initForm() {
  const SUCCESS_MESSAGE = 'Survey submitted, thank you!';
  const FAILURE_MESSAGE = 'There was a problem with submitting the survey. Please try again later.';
  const ALL_RESPONSES_MISSING_MESSAGE = 'At least one field must be populated';
  const submitButton = document.getElementById('submit-button');

  submitButton.addEventListener('click', async () => {
    const form = createForm(getContactDetails(), getFormResponses(), getFeedback);
    if (formIsUnpopulated(form)) {
      document.getElementById('survey-feedback').innerText = ALL_RESPONSES_MISSING_MESSAGE;
      return;
    }
    await sendForm(form)
      .then(() => {
        document.getElementById('survey-feedback').innerText = SUCCESS_MESSAGE;
      })
      .catch(() => {
        document.getElementById('survey-feedback').innerText = FAILURE_MESSAGE;
      });
  });
}

module.exports = {
  getFormResponses,
  getContactDetails,
  getFeedback,
  createForm,
  sendForm,
  generateResponseObject,
  formIsUnpopulated,
  initForm,
};
