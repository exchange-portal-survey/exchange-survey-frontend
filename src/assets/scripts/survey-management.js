const { createElement } = require('./helpers/element-factory');
const { createChart, getDatasetFromDataValues } = require('./helpers/chart-helper');

function checkIfAuthorised() {
  const authToken = localStorage.getItem('exchange-survey-token');
  let authorised = false;

  if (authToken) {
    const tokenDate = new Date(authToken);
    const now = new Date();

    const msBetweenDates = Math.abs(tokenDate.getTime() - now.getTime());
    const hoursBetweenDates = msBetweenDates / (60 * 60 * 1000);

    if (hoursBetweenDates < 24) {
      authorised = true;
    }
  }
  return authorised;
}

async function getResponseCounts(startDate, endDate) {
  let response;
  if (startDate && endDate) {
    response = await fetch(`http://localhost:3000/survey/responseCount?start=${startDate}&end=${endDate}`);
  } else {
    response = await fetch('http://localhost:3000/survey/responseCount');
  }

  return response.json();
}

async function getSurveyResponses(startDate, endDate) {
  let response;
  if (startDate && endDate) {
    response = await fetch(`http://localhost:3000/survey/all?start=${startDate}&end=${endDate}`);
  } else {
    response = await fetch('http://localhost:3000/survey/all');
  }
  return response.json();
}

function getRoleText(role) {
  let roleText;
  switch (role) {
    case 'product-manager': {
      roleText = 'Product Manager';
      break;
    }
    case 'product-owner': {
      roleText = 'Product Owner';
      break;
    }
    case 'delivery-manager': {
      roleText = 'Delivery Manager';
      break;
    }
    case 'business-analyst': {
      roleText = 'Business Analyst';
      break;
    }
    case 'software-engineer': {
      roleText = 'Software Engineer';
      break;
    }
    case 'other': {
      roleText = 'Other';
      break;
    }
    default: roleText = '';
  }
  return roleText;
}

function createContactDetailsContainer(surveyResponse) {
  const contactContainer = createElement('div', null, 'govuk-grid-column-one-third', null);
  const contactDetailsHeading = createElement('h3', 'Contact details', null, null);
  const email = createElement('p', `Email: ${surveyResponse.contact.email}`, null, null);
  const role = createElement('p', `Role: ${getRoleText(surveyResponse.contact.role)}`, null, null);
  const dateCreated = createElement('p', `Date: ${new Date(surveyResponse.createdAt).toLocaleDateString('en-gb')}`, null, null);
  contactContainer.appendChild(contactDetailsHeading);
  contactContainer.appendChild(email);
  contactContainer.appendChild(role);
  contactContainer.appendChild(dateCreated);
  return contactContainer;
}

function createResponseContainer(surveyResponse) {
  const responseAndFeedbackContainer = createElement('div', null, 'govuk-grid-column-two-thirds', null);
  const responsesHeading = createElement('h3', 'Responses', null, null);
  responseAndFeedbackContainer.appendChild(responsesHeading);
  surveyResponse.responses.forEach((response) => {
    const question = createElement('p', response.question, 'govuk-!-font-weight-bold', null);
    const responseVal = response.response ?? 0;
    const responseElem = createElement('p', responseVal, 'response-val', null);
    responseAndFeedbackContainer.appendChild(question);
    responseAndFeedbackContainer.appendChild(responseElem);
  });
  const feedbackHeading = createElement('p', 'Feedback', 'govuk-!-font-weight-bold', null);
  const feedback = createElement('p', surveyResponse.feedback ?? '', 'feedback-val', null);
  responseAndFeedbackContainer.appendChild(feedbackHeading);
  responseAndFeedbackContainer.appendChild(feedback);
  return responseAndFeedbackContainer;
}

function createResponseElem(surveyResponse, container) {
  const responseContainer = createElement('div', null, 'govuk-grid-column-full app-all-responses__response-container govuk-body', null);
  const contactContainer = createContactDetailsContainer(surveyResponse);
  const responsesContainer = createResponseContainer(surveyResponse);
  responseContainer.appendChild(contactContainer);
  responseContainer.appendChild(responsesContainer);
  container.appendChild(responseContainer);
}

function displaySurveyResponses(surveyResponses) {
  const container = document.getElementById('responses-container');
  container.innerHTML = '';
  if (surveyResponses.length > 0) {
    surveyResponses.slice().reverse().forEach((surveyResponse) => {
      createResponseElem(surveyResponse, container);
    });
  } else {
    const message = createElement('p', 'There are no responses', 'govuk-body-m', null);
    container.appendChild(message);
  }
}

async function displayResponseGraphs(responseCounts) {
  const container = document.getElementById('survey-responses');
  container.innerHTML = '';
  const responseCountArr = await responseCounts;
  let questionNumber = 1;
  if (responseCountArr && responseCountArr.length !== 0) {
    responseCountArr.forEach((response) => {
      const divElem = createElement('div', null, 'govuk-grid-column-one-half govuk-body', `question-${questionNumber}-container`);
      const question = createElement('p', response.question, 'govuk-!-font-weight-bold');
      divElem.appendChild(question);
      const chart = createElement('canvas', null, 'app-chart', null);
      const labels = [1, 2, 3, 4, 5];
      const dataset = getDatasetFromDataValues(response.counts, labels);
      createChart(chart, dataset, response.question, 'doughnut', labels);
      divElem.appendChild(chart);
      container.appendChild(divElem);
      questionNumber += 1;
    });
  } else {
    const errorMessage = 'Sorry, there are no responses';
    const errorMessageElem = createElement('p', errorMessage, 'govuk-body');
    container.appendChild(errorMessageElem);
  }
}

async function initDateUpdateButton() {
  const dateSubmitButton = document.getElementById('submit-button');
  dateSubmitButton.addEventListener('click', async () => {
    const startDate = document.getElementById('start-date').value;
    const endDate = document.getElementById('end-date').value;
    const responsesByDate = getResponseCounts(startDate, endDate);
    const surveyResponses = await getSurveyResponses(startDate, endDate);
    await displayResponseGraphs(responsesByDate);
    await displaySurveyResponses(surveyResponses);
  });
}

function setDateMin() {
  document.getElementById('start-date').min = '2022-01-01';
  document.getElementById('end-date').min = '2022-01-01';
  document.getElementById('start-date').value = '2022-01-01';
}

function setDateMax() {
  document.getElementById('start-date').max = new Date().toLocaleDateString('en-ca');
  document.getElementById('end-date').max = new Date().toLocaleDateString('en-ca');
  document.getElementById('end-date').value = new Date().toLocaleDateString('en-ca');
}

async function setNumberOfResponses() {
  try {
    const response = await fetch('http://localhost:3000/survey/responses/count');
    const responseObject = await response.json();
    document.getElementById('number-of-surveys-completed').innerText = responseObject.numberOfResponses;
  } catch (error) {
    document.getElementById('number-of-surveys-completed').innerText = 'Could not retrieve survey responses';
  }
}

async function initResetButton(responseCounts, surveyResponses) {
  const resetButton = document.getElementById('reset-button');
  resetButton.addEventListener('click', async () => {
    setDateMin();
    setDateMax();
    await displayResponseGraphs(responseCounts);
    await displaySurveyResponses(surveyResponses);
  });
}

async function initSurveyManagement() {
  const isAuthorised = checkIfAuthorised();
  if (!isAuthorised) {
    window.location.href = '/authenticate';
  }
  const responseCounts = getResponseCounts();
  const surveyResponses = await getSurveyResponses();
  await setNumberOfResponses();
  await displayResponseGraphs(responseCounts);
  await displaySurveyResponses(surveyResponses);
  setDateMin();
  setDateMax();
  await initDateUpdateButton();
  await initResetButton(responseCounts, surveyResponses);
}

module.exports = {
  checkIfAuthorised,
  initSurveyManagement,
  setDateMax,
  setDateMin,
  setNumberOfResponses,
  getResponseCounts,
  displayResponseGraphs,
  getRoleText,
  getSurveyResponses,
  displaySurveyResponses,
  createContactDetailsContainer,
  createResponseContainer,
  createResponseElem,
};
