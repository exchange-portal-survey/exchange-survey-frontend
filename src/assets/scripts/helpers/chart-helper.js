const Chart = require('chart.js');

function getDatasetFromDataValues(data, labels) {
  const dataset = [];
  labels.forEach((responseVal) => {
    const val = data.find((dataPoint) => responseVal === dataPoint.response)?.count ?? 0;
    dataset.push(val);
  });
  return dataset;
}

function createChart(chartElem, data, label, chartType, labels) {
  return new Chart(chartElem, {
    type: chartType,
    data: {
      labels,
      datasets: [{
        label,
        data,
        backgroundColor: [
          'rgba(212, 53, 28, 0.5)',
          'rgba(244, 119, 56,0.5)',
          'rgba(255, 221, 0, 0.5)',
          'rgba(86, 148, 202, 0.5)',
          'rgba(0, 112, 60, 0.5)',
        ],
        borderColor: [
          'rgba(212, 53, 28, 1)',
          'rgba(244, 119, 56, 1)',
          'rgba(255, 221, 0, 1)',
          'rgba(86, 148, 202, 1)',
          'rgba(0, 112, 60, 1)',
        ],
      }],
    },
    options: {
      responsive: true,
    },
  });
}

module.exports = { createChart, getDatasetFromDataValues };
