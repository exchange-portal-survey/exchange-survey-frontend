function createElement(tagType, innerText, classList, id) {
  const element = document.createElement(tagType);
  element.innerText = innerText;

  if (id) {
    element.id = id;
  }
  if (classList) {
    element.classList = classList;
  }

  return element;
}

module.exports = { createElement };
