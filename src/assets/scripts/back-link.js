function initBackLink() {
  const backLink = document.getElementById('back-link');
  backLink.addEventListener('click', () => {
    // eslint-disable-next-line no-restricted-globals
    history.back();
  });
}

module.exports = { initBackLink };
