const { initAuth } = require('./authenticate');
const { initBackLink } = require('./back-link');
const { initForm } = require('./form');
const { initSurveyManagement } = require('./survey-management');

document.addEventListener('DOMContentLoaded', async () => {
  initBackLink();
  if (document.getElementById('survey')) {
    initForm();
  } else if (document.getElementById('authenticate')) {
    await initAuth();
  } else if (document.getElementById('survey-management')) {
    await initSurveyManagement();
  }
});
