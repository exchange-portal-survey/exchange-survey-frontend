import gulp from 'gulp';
import gulpSass from 'gulp-sass';
import sassModule from 'sass';
import del from 'del';

const sass = gulpSass(sassModule);

gulp.task('styles', () => gulp.src('./src/assets/styles/*.scss')
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(gulp.dest('./src/assets/dwp/styles')));

gulp.task('copyFonts', () => gulp.src('./node_modules/govuk-frontend/govuk/assets/fonts/*.*')
  .pipe(gulp.dest('./src/assets/dwp/fonts')));

gulp.task('copyImages', () => gulp.src('./node_modules/govuk-frontend/govuk/assets/images/*.*')
  .pipe(gulp.dest('./src/assets/dwp/images')));

gulp.task('copyJavascript', () => gulp.src('./node_modules/govuk-frontend/govuk/all.js')
  .pipe(gulp.dest('./src/assets/dwp/js')));

gulp.task('clean', () => del([
  './src/assets/dwp/styles/main.css',
  './src/assets/dwp/fonts/*.*',
  './src/assets/dwp/images/*.*',
  './src/assets/dwp/js/*.*',
]));

gulp.task('default', gulp.series(
  ['clean', 'styles', 'copyFonts', 'copyImages', 'copyJavascript'],
));
