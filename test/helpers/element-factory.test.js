const { createElement } = require('../../src/assets/scripts/helpers/element-factory');

describe('Create DOM element', () => {
  let testCreateElement;

  beforeEach(() => {
    testCreateElement = jest.fn(createElement);
  });

  it('should return a div element with a class and no internal text', () => {
    const elemType = 'div';
    const innerText = null;
    const classList = 'test-class';

    const createdElem = testCreateElement(elemType, innerText, classList);
    expect(createdElem.innerText).toBeNull();
    expect(createdElem.classList).toContain('test-class');
    expect(createdElem.tagName.toLowerCase()).toEqual('div');
  });

  it('should return a h1 element with a class and internal text', () => {
    const elemType = 'h1';
    const innerText = 'test-heading';
    const classList = 'test-class';

    const createdElem = testCreateElement(elemType, innerText, classList);
    expect(createdElem.innerText).toEqual('test-heading');
    expect(createdElem.classList).toContain('test-class');
    expect(createdElem.tagName.toLowerCase()).toEqual('h1');
  });
});
