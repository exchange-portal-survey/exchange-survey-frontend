const {
  createChart,
  getDatasetFromDataValues,
} = require('../../src/assets/scripts/helpers/chart-helper');
const { createElement } = require('../../src/assets/scripts/helpers/element-factory');

describe('Chart helper', () => {
  let testCreateChart;
  let chartElem;

  const data = [25, 21, 13, 10, 1];
  const labels = [1, 2, 3, 4, 5];
  const label = 'label';

  beforeEach(() => {
    testCreateChart = jest.fn(createChart);
    chartElem = createElement('canvas', null, 'app-chart', null);
  });

  it('should return a chart of the correct type', () => {
    const chartType = 'pie';
    const chart = testCreateChart(chartElem, data, label, chartType, labels);
    // eslint-disable-next-line no-underscore-dangle
    const actualType = chart.config._config.type;
    expect(actualType).toEqual(chartType);
  });

  it('should return a chart with correct label', () => {
    const chartType = 'pie';
    const chart = testCreateChart(chartElem, data, label, chartType, labels);
    // eslint-disable-next-line no-underscore-dangle
    const actualLabel = chart.config._config.data.datasets[0].label;
    expect(actualLabel).toEqual(label);
  });

  it('should return a chart with correct data', () => {
    const chartType = 'pie';
    const chart = testCreateChart(chartElem, data, label, chartType, labels);
    const expectedData = [25, 21, 13, 10, 1];
    // eslint-disable-next-line no-underscore-dangle
    const actualData = chart.config._config.data.datasets[0].data;
    expect(expectedData).toEqual(actualData);
  });
});

describe('Transform datapoints into dataset for chart creation', () => {
  const labels = [1, 2, 3, 4, 5];
  let testGetDatasetFromDataValues;

  beforeEach(() => {
    testGetDatasetFromDataValues = jest.fn(getDatasetFromDataValues);
  });

  it('should return an array of integers of correct count values', () => {
    const datapoints = [
      {
        response: 1,
        count: 62,
      },
      {
        response: 2,
        count: 5,
      },
      {
        response: 3,
        count: 10,
      },
      {
        response: 4,
        count: 21,
      },
      {
        response: 5,
        count: 15,
      },
    ];
    const expectedDataset = [62, 5, 10, 21, 15];
    const actualDataset = testGetDatasetFromDataValues(datapoints, labels);
    expect(actualDataset).toEqual(expectedDataset);
  });

  it('should populate array with 0 if there is no response value', () => {
    const datapoints = [
      {
        response: 2,
        count: 5,
      },
      {
        response: 3,
        count: 10,
      },
      {
        response: 4,
        count: 21,
      },
      {
        response: 5,
        count: 15,
      },
    ];
    const expectedDataset = [0, 5, 10, 21, 15];
    const actualDataset = testGetDatasetFromDataValues(datapoints, labels);
    expect(actualDataset).toEqual(expectedDataset);
  });
});
