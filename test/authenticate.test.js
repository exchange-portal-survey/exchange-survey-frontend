const fs = require('fs');
const path = require('path');
const fetchMock = require('jest-fetch-mock');
const { authenticate, setAuthToken, getUserInput } = require('../src/assets/scripts/authenticate');

const getHTMLData = (file) => fs.readFileSync(
  path.resolve(__dirname, '../test/assets', `${file}`),
  'utf8',
  (err, data) => {
    if (err) {
      throw err;
    }

    return data.toString();
  },
);

fetchMock.enableFetchMocks();

beforeEach(() => {
  document.body.innerHTML = getHTMLData('authenticate.test.html');
  fetch.resetMocks();
});

describe('Authentication function', () => {
  let testAuthenticate;
  beforeEach(() => {
    testAuthenticate = jest.fn(authenticate);
  });

  it('should return true when correct password inputted', async () => {
    const response = true;
    fetch.mockResponseOnce(response);

    const userInput = 'test-password';
    const result = await testAuthenticate(userInput);
    expect(result).toBeTruthy();
  });

  it('should return false when correct password inputted', async () => {
    const response = false;
    fetch.mockResponseOnce(response);

    const userInput = 'incorrect-test-password';
    const result = await testAuthenticate(userInput);
    expect(result).toBeFalsy();
  });
});

describe('Authentication stores token', () => {
  beforeEach(() => {
    localStorage.clear();
    jest.useFakeTimers().setSystemTime(new Date('2022-10-06:12:00:00Z'));
  });

  it('should set a token with the current time', () => {
    const testSetAuthToken = jest.fn(setAuthToken);
    testSetAuthToken();

    const token = localStorage.getItem('exchange-survey-token');
    const expectedValue = '2022-10-06T12:00:00';
    expect(token).toContain(expectedValue);
  });
});

describe('Retrieve user password input', () => {
  it('should return the password entered by the user as a string', () => {
    const testGetUserInput = jest.fn(getUserInput);
    const elem = document.getElementById('password');
    const userValue = 'test-password';
    elem.value = userValue;

    const result = testGetUserInput();
    expect(result).toEqual(userValue);
  });
});

describe('Setting authentication token', () => {
  it('should set the token to the current date', () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-10-06:12:00:00Z'));
    const testSetAuthToken = jest.fn(setAuthToken);
    const expectedValue = '2022-10-06T12:00:00.000Z';

    testSetAuthToken();
    const actualValue = localStorage.getItem('exchange-survey-token');

    expect(actualValue).toEqual(expectedValue);
    localStorage.clear();
  });
});
