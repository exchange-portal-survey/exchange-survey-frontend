const fs = require('fs');
const path = require('path');
const fetchMock = require('jest-fetch-mock');
const {
  checkIfAuthorised,
  setDateMin,
  setDateMax,
  setNumberOfResponses,
  getResponseCounts,
  displayResponseGraphs,
  getRoleText,
  getSurveyResponses,
  createResponseContainer,
  createContactDetailsContainer,
  createResponseElem,
  displaySurveyResponses,
} = require('../src/assets/scripts/survey-management');
const { createElement } = require('../src/assets/scripts/helpers/element-factory');

const getHTMLData = (file) => fs.readFileSync(
  path.resolve(__dirname, '../test/assets', `${file}`),
  'utf8',
  (err, data) => {
    if (err) {
      throw err;
    }

    return data.toString();
  },
);

fetchMock.enableFetchMocks();

describe('Check if user is authorised', () => {
  let testCheckIfAuthorised;

  beforeEach(() => {
    jest.useFakeTimers().setSystemTime(new Date('2022-10-06:12:00:00Z'));
    localStorage.clear();

    testCheckIfAuthorised = jest.fn(checkIfAuthorised);
  });

  it('should return true if user has been authorised', () => {
    const validAuthDate = '2022-10-06T11:00:00Z';
    localStorage.setItem('exchange-survey-token', validAuthDate);

    const result = testCheckIfAuthorised();
    expect(result).toBeTruthy();
  });

  it('should return false if user has no token', () => {
    const result = testCheckIfAuthorised();
    expect(result).toBeFalsy();
  });

  it('should return false if user has an expired token', () => {
    const invalidAuthDate = '2022-09-06T11:00:00Z';
    localStorage.setItem('exchange-survey-token', invalidAuthDate);

    const result = testCheckIfAuthorised();
    expect(result).toBeFalsy();
  });
});

describe('Setting filter date input to maximum value', () => {
  let testSetDateMax;

  beforeEach(() => {
    testSetDateMax = jest.fn(setDateMax);
    document.body.innerHTML = getHTMLData('survey-management.test.html');
    jest.useFakeTimers().setSystemTime(new Date('2022-10-06:12:00:00Z'));
  });

  it('should set the start date maximum value to the current date', () => {
    testSetDateMax();
    const startDateElem = document.getElementById('start-date');
    expect(startDateElem.max).toEqual('2022-10-06');
  });

  it('should set the end date maximum value to the current date', () => {
    testSetDateMax();
    const endDateElem = document.getElementById('end-date');
    expect(endDateElem.max).toEqual('2022-10-06');
  });

  it('should set the end date value to the current date', () => {
    testSetDateMax();
    const endDateElem = document.getElementById('end-date');
    expect(endDateElem.value).toEqual('2022-10-06');
  });
});

describe('Setting filter date input to minimum value', () => {
  let testSetDateMin;

  beforeEach(() => {
    testSetDateMin = jest.fn(setDateMin);
    document.body.innerHTML = getHTMLData('survey-management.test.html');
  });

  it('should set the start date minimum value to 2022-01-01', () => {
    testSetDateMin();
    const startDateElem = document.getElementById('start-date');
    expect(startDateElem.min).toEqual('2022-01-01');
  });

  it('should set the end date minimum value to 2022-01-01', () => {
    testSetDateMin();
    const startDateElem = document.getElementById('start-date');
    expect(startDateElem.min).toEqual('2022-01-01');
  });

  it('should set the start date value to 2022-01-01', () => {
    testSetDateMin();
    const startDateElem = document.getElementById('start-date');
    expect(startDateElem.value).toEqual('2022-01-01');
  });
});

describe('Displaying number of completed surveys', () => {
  let testSetNumberOfResponses;

  beforeEach(() => {
    testSetNumberOfResponses = jest.fn(setNumberOfResponses);
    document.body.innerHTML = getHTMLData('survey-management.test.html');
  });

  afterEach(() => {
    fetchMock.resetMocks();
  });

  it('should display 5 completed surveys when 5 responses recieved from backend', async () => {
    fetch.mockResponseOnce(JSON.stringify({ numberOfResponses: 5 }));
    await testSetNumberOfResponses();
    const surveysCompletedElem = document.getElementById('number-of-surveys-completed');
    expect(fetch).toHaveBeenCalledWith('http://localhost:3000/survey/responses/count');
    expect(surveysCompletedElem.innerText).toEqual(5);
  });
});

describe('Retrieve response counts from backend API', () => {
  let testGetResponseCounts;

  beforeEach(() => {
    testGetResponseCounts = jest.fn(getResponseCounts);
  });

  afterEach(() => {
    fetchMock.resetMocks();
  });

  it('should call the correct url when no startDate or endDate passed', () => {
    fetch.mockResponseOnce(JSON.stringify({}));
    testGetResponseCounts();
    expect(fetch).toHaveBeenCalledWith('http://localhost:3000/survey/responseCount');
  });

  it('should call the correct url with start and end date when arguments passed', () => {
    fetch.mockResponseOnce(JSON.stringify({}));
    const startDate = '2022-10-10';
    const endDate = '2022-10-12';
    testGetResponseCounts(startDate, endDate);
    expect(fetch).toHaveBeenCalledWith(`http://localhost:3000/survey/responseCount?start=${startDate}&end=${endDate}`);
  });
});

describe('Displaying response graphs', () => {
  let testDisplayResponseGraphs;

  beforeEach(() => {
    testDisplayResponseGraphs = jest.fn(displayResponseGraphs);
    document.body.innerHTML = getHTMLData('survey-management.test.html');
    // mock for showing graphs in tests
    global.ResizeObserver = jest.fn().mockImplementation(() => ({
      observe: jest.fn(),
      unobserve: jest.fn(),
      disconnect: jest.fn(),
    }));
  });

  it('should display a message saying there are no responses when the response array is empty', async () => {
    const noResponses = [];
    await testDisplayResponseGraphs(noResponses);
    const responsesContainer = document.getElementById('survey-responses');
    const expectedMessage = 'Sorry, there are no responses';

    expect(responsesContainer.children[0].innerText).toEqual(expectedMessage);
  });

  it('should display four elements when four responses passed', async () => {
    const responses = [
      {
        question: 'How easy was it to locate assets on the Portal?',
        counts: [
          {
            response: 5,
            count: 7,
          },
        ],
      },
      {
        question: 'How easy was it to understand the Portal content?',
        counts: [
          {
            response: 2,
            count: 7,
          },
        ],
      },
      {
        question: 'How would you rate the experience of completing the consume form?',
        counts: [
          {
            response: 3,
            count: 6,
          },
          {
            response: 4,
            count: 1,
          },
        ],
      },
      {
        question: 'What would you rate the quality of the documentation in the Portal?',
        counts: [
          {
            response: 4,
            count: 7,
          },
        ],
      },
    ];
    document.addEventListener('DOMContentLoaded', async () => {
      await testDisplayResponseGraphs(responses);
      const responsesContainer = document.getElementById('survey-responses');

      const firstResponse = responsesContainer.children[0];
      const secondResponse = responsesContainer.children[1];
      const thirdResponse = responsesContainer.children[2];
      const fourthResponse = responsesContainer.children[3];

      const firstResponseHeading = firstResponse.getElementsByTagName('p')[0];
      const secondResponseHeading = secondResponse.getElementsByTagName('p')[0];
      const thirdResponseHeading = thirdResponse.getElementsByTagName('p')[0];
      const fourthResponseHeading = fourthResponse.getElementsByTagName('p')[0];

      expect(firstResponseHeading.innerText).toEqual(responses[0].question);
      expect(secondResponseHeading.innerText).toEqual(responses[1].question);
      expect(thirdResponseHeading.innerText).toEqual(responses[2].question);
      expect(fourthResponseHeading.innerText).toEqual(responses[3].question);
      expect(responsesContainer.childElementCount).toEqual(4);
    });
  });
});

describe('Get the survey response contact details role text', () => {
  let testGetRoleText;

  beforeEach(() => {
    testGetRoleText = jest.fn(getRoleText);
  });

  afterEach(() => {
    fetchMock.resetMocks();
  });

  it('should return Product Owner when passed product-owner', () => {
    const expected = 'Product Owner';
    const actual = testGetRoleText('product-owner');
    expect(actual).toEqual(expected);
  });

  it('should return Product Manager when passed product-manager', () => {
    const expected = 'Product Manager';
    const actual = testGetRoleText('product-manager');
    expect(actual).toEqual(expected);
  });

  it('should return Delivery Manager when passed delivery-manager', () => {
    const expected = 'Delivery Manager';
    const actual = testGetRoleText('delivery-manager');
    expect(actual).toEqual(expected);
  });

  it('should return Business Analyst when passed business-analyst', () => {
    const expected = 'Business Analyst';
    const actual = testGetRoleText('business-analyst');
    expect(actual).toEqual(expected);
  });

  it('should return Software Engineer when passed software-engineer', () => {
    const expected = 'Software Engineer';
    const actual = testGetRoleText('software-engineer');
    expect(actual).toEqual(expected);
  });

  it('should return Other when passed other', () => {
    const expected = 'Other';
    const actual = testGetRoleText('other');
    expect(actual).toEqual(expected);
  });

  it('should return an empty string when passed any other values', () => {
    const expected = '';
    const actual = testGetRoleText('unexpected-value');
    expect(actual).toEqual(expected);
  });
});

describe('Returns all response documents from backend', () => {
  let testGetSurveyResponses;

  beforeEach(() => {
    testGetSurveyResponses = jest.fn(getSurveyResponses);
  });

  afterEach(() => {
    fetchMock.resetMocks();
  });

  it('should call the backend api route once with fetch', () => {
    const response = {
      contact: {
        email: 'test@email.com',
      },
      responses: [
        {
          question: 'test question',
          response: 4,
        },
      ],
    };
    fetch.mockResponseOnce(JSON.stringify(response));
    testGetSurveyResponses();
    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith('http://localhost:3000/survey/all');
  });

  it('should return a JSON of the responses', async () => {
    const expectedResponse = {
      contact: {
        email: 'test@email.com',
      },
      responses: [
        {
          question: 'test question',
          response: 4,
        },
      ],
    };
    fetch.mockResponseOnce(JSON.stringify(expectedResponse));

    const response = await testGetSurveyResponses();
    expect(response).toEqual(expectedResponse);
  });

  it('should call the API with the startDate and endDate when these arguments passed', async () => {
    const expectedResponse = {
      contact: {
        email: 'test@email.com',
      },
      responses: [
        {
          question: 'test question',
          response: 4,
        },
      ],
    };
    fetch.mockResponseOnce(JSON.stringify(expectedResponse));

    const startDate = '2022-10-13';
    const endDate = '2022-10-14';

    const response = await testGetSurveyResponses(startDate, endDate);
    expect(response).toEqual(expectedResponse);
    expect(fetch).toHaveBeenCalledWith(`http://localhost:3000/survey/all?start=${startDate}&end=${endDate}`);
  });
});

describe('Create contact details element from survey response', () => {
  it('should return a div element with email and role', () => {
    const testCreateContactDetailsContainer = jest.fn(createContactDetailsContainer);
    const surveyResponse = {
      contact: {
        email: 'test@email.com',
        role: 'product-owner',
      },
      responses: [
        {
          question: 'test question',
          response: 5,
        },
      ],
    };
    const result = testCreateContactDetailsContainer(surveyResponse);
    const headingElem = [...result.getElementsByTagName('h3')][0];
    const paragraphElems = [...result.getElementsByTagName('p')];

    expect(headingElem.innerText).toEqual('Contact details');
    expect(paragraphElems[0].innerText).toEqual('Email: test@email.com');
    expect(paragraphElems[1].innerText).toEqual('Role: Product Owner');
  });
});

describe('Creates response container', () => {
  const surveyResponse = {
    contact: {
      email: 'test@email.com',
      role: 'product-owner',
    },
    responses: [
      {
        question: 'test question',
        response: 5,
      },
      {
        question: 'test question 2',
        response: 6,
      },
      {
        question: 'test question 3',
        response: 10,
      },
    ],
    feedback: 'test feedback',
  };

  it('should return a div element containing the response questions', () => {
    const testCreateResponseContainer = jest.fn(createResponseContainer);
    const result = testCreateResponseContainer(surveyResponse);
    const headingElem = [...result.getElementsByTagName('h3')][0];
    const paragraphElems = [...result.getElementsByTagName('p')];

    expect(headingElem.innerText).toEqual('Responses');
    expect(paragraphElems.length).toEqual(8);
    expect(paragraphElems[0].innerText).toEqual('test question');
    expect(paragraphElems[1].innerText).toEqual(5);
    expect(paragraphElems[2].innerText).toEqual('test question 2');
    expect(paragraphElems[3].innerText).toEqual(6);
    expect(paragraphElems[4].innerText).toEqual('test question 3');
    expect(paragraphElems[5].innerText).toEqual(10);
    expect(paragraphElems[6].innerText).toEqual('Feedback');
    expect(paragraphElems[7].innerText).toEqual('test feedback');
  });
});

describe('Creates full response container from a survey response', () => {
  const surveyResponse = {
    contact: {
      email: 'test@email.com',
      role: 'product-owner',
    },
    responses: [
      {
        question: 'test question',
        response: 5,
      },
      {
        question: 'test question 2',
        response: 6,
      },
      {
        question: 'test question 3',
        response: 10,
      },
    ],
    feedback: 'test feedback',
  };

  let testCreateResponseElem;

  beforeEach(() => {
    testCreateResponseElem = jest.fn(createResponseElem);
  });

  it('should create the response and add it to the parent container', () => {
    const container = createElement('div', null, null, null);
    testCreateResponseElem(surveyResponse, container);

    const responseContainer = [...container.children][0];
    const contactDetailsContainer = [...responseContainer.children][0];
    const allResponsesContainer = [...responseContainer.children][1];

    const contactHeadingElem = [...contactDetailsContainer.getElementsByTagName('h3')][0];
    const contactParagraphElems = [...contactDetailsContainer.getElementsByTagName('p')];

    const responsesHeadingElem = [...allResponsesContainer.getElementsByTagName('h3')][0];
    const responsesParagraphElems = [...allResponsesContainer.getElementsByTagName('p')];

    expect(responsesHeadingElem.innerText).toEqual('Responses');
    expect(responsesParagraphElems[0].innerText).toEqual('test question');
    expect(responsesParagraphElems[1].innerText).toEqual(5);
    expect(responsesParagraphElems[2].innerText).toEqual('test question 2');
    expect(responsesParagraphElems[3].innerText).toEqual(6);
    expect(responsesParagraphElems[4].innerText).toEqual('test question 3');
    expect(responsesParagraphElems[5].innerText).toEqual(10);
    expect(responsesParagraphElems[6].innerText).toEqual('Feedback');
    expect(responsesParagraphElems[7].innerText).toEqual('test feedback');
    expect(contactHeadingElem.innerText).toEqual('Contact details');
    expect(contactParagraphElems[0].innerText).toEqual('Email: test@email.com');
    expect(contactParagraphElems[1].innerText).toEqual('Role: Product Owner');
  });
});

describe('Show all survey responses', () => {
  let testDisplaySurveyResponses;

  beforeEach(() => {
    testDisplaySurveyResponses = jest.fn(displaySurveyResponses);
    document.body.innerHTML = getHTMLData('all-survey-responses.test.html');
  });

  it('should have two response container elements when two survey responses completed', () => {
    const surveyResponses = [
      {
        contact: {
          email: 'test@email.com',
          role: 'product-owner',
        },
        responses: [
          {
            question: 'test question',
            response: 5,
          },
          {
            question: 'test question 2',
            response: 6,
          },
          {
            question: 'test question 3',
            response: 10,
          },
        ],
        feedback: 'test feedback',
      },
      {
        contact: {
          email: 'test@email.com',
          role: 'product-owner',
        },
        responses: [
          {
            question: 'test question',
            response: 5,
          },
          {
            question: 'test question 2',
            response: 6,
          },
          {
            question: 'test question 3',
            response: 10,
          },
        ],
        feedback: 'test feedback',
      },
    ];
    testDisplaySurveyResponses(surveyResponses);
    const containerElem = document.getElementById('responses-container');
    expect(containerElem.childElementCount).toEqual(2);
  });

  it('should add a message to the page saying there are no responses when passed an empty array', () => {
    const emptySurveyRespones = [];
    testDisplaySurveyResponses(emptySurveyRespones);
    const containerElem = document.getElementById('responses-container');
    expect(containerElem.childElementCount).toEqual(1);
    expect(containerElem.children[0].innerText).toEqual('There are no responses');
  });
});
