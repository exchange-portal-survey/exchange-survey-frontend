const fs = require('fs');
const path = require('path');
const fetchMock = require('jest-fetch-mock');
const {
  getContactDetails,
  getFormResponses,
  generateResponseObject,
  sendForm,
} = require('../src/assets/scripts/form');
const { getFeedback, createForm } = require('../src/assets/scripts/form');
const { formIsUnpopulated } = require('../src/assets/scripts/form');

const getHTMLData = (file) => fs.readFileSync(
  path.resolve(__dirname, '../test/assets', `${file}`),
  'utf8',
  (err, data) => {
    if (err) {
      throw err;
    }

    return data.toString();
  },
);

fetchMock.enableFetchMocks();

beforeEach(() => {
  document.body.innerHTML = getHTMLData('index.test.html');
  fetch.resetMocks();
});

describe('Get contact details from populated form', () => {
  it('should return the email and role', () => {
    const testGetContactDetails = jest.fn(getContactDetails);
    const contactDetails = testGetContactDetails();
    const expectedContactDetails = {
      email: 'test@email.com',
      role: 'software-engineer',
    };
    expect(contactDetails).toEqual(expectedContactDetails);
  });
});

describe('Get responses from survey', () => {
  it('should return the question names and the user responses', () => {
    const testGetFormResponses = jest.fn(getFormResponses);
    const formResponses = testGetFormResponses();
    const expectedFormResponses = [
      {
        question: 'How would you rate the experience of locating assets on the Portal?',
        response: '1',
      },
      {
        question: 'How would you rate the experience of completing the consume form?',
        response: '3',
      },
      {
        question: 'How would you rate the quality of the Portal content?',
        response: '5',
      },
      {
        question: 'How would you rate the quality of the Business and Technical documentation on the Portal?',
        response: '2',
      },
    ];

    expect(formResponses).toEqual(expectedFormResponses);
  });
});

describe('Create object from form', () => {
  it('should return an object with the question text and the response', () => {
    const testGenerateResponseObject = jest.fn(generateResponseObject);
    const response = document.getElementsByClassName('app-survey__question')[0];
    const responseObject = testGenerateResponseObject(response, 1);
    const expectedResponseObject = {
      question: 'How would you rate the experience of locating assets on the Portal?',
      response: '1',
    };

    expect(responseObject).toEqual(expectedResponseObject);
  });
});

describe('Send form to API', () => {
  it('should send the form object to the backend API', async () => {
    fetch.mockResponseOnce('Request successful');
    const testSendForm = jest.fn(sendForm);
    const mockForm = {
      question: 'test question',
      response: '5',
    };
    const response = await testSendForm(mockForm);
    expect(response.body.toString()).toEqual('Request successful');
    expect(fetch).toHaveBeenCalledTimes(1);
  });
});

describe('Retrieve feedback from page', () => {
  it('should return the feedback string', () => {
    const testGetFeedback = jest.fn(getFeedback);
    const feedbackElem = document.getElementById('feedback');
    const userInput = 'test feedback';

    feedbackElem.value = userInput;
    expect(testGetFeedback()).toEqual(userInput);
  });
});

describe('Creates a form object with correct values', () => {
  it('should return an object with the contact details, form response, and feedback', () => {
    const feedback = 'test feedback';
    const testCreateForm = jest.fn(createForm);
    const contactDetails = {
      email: 'test@email.com',
      role: 'product-manager',
    };
    const formResponses = [
      {
        question: 'test question',
        response: 5,
      },
    ];
    const result = testCreateForm(
      contactDetails,
      formResponses,
      jest.fn(getFeedback).mockReturnValue(feedback),
    );
    const expectedResult = {
      contact: contactDetails,
      responses: formResponses,
      feedback,
    };
    expect(result).toEqual(expectedResult);
  });
});

describe('Validate form has at least one field populated', () => {
  let testFormIsUnpopulated;

  beforeEach(() => {
    testFormIsUnpopulated = jest.fn(formIsUnpopulated);
  });

  it('should return true when all fields unpopulated', () => {
    const form = {
      responses: [
        {
          question: 'test question',
          response: undefined,
        },
      ],
      feedback: '',
    };

    const result = testFormIsUnpopulated(form);
    expect(result).toBeTruthy();
  });

  it('should return false when one response field populated', () => {
    const form = {
      responses: [
        {
          question: 'test question',
          response: 2,
        },
      ],
      feedback: '',
    };

    const result = testFormIsUnpopulated(form);
    expect(result).toBeFalsy();
  });

  it('should return false when all response field unpopulated but feedback is populated', () => {
    const form = {
      responses: [
        {
          question: 'test question',
          response: undefined,
        },
      ],
      feedback: 'test feedback',
    };

    const result = testFormIsUnpopulated(form);
    expect(result).toBeFalsy();
  });
});
